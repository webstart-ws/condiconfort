$(document).ready(() => {
    $('.customers_comment_block').slick({
        dots: false,
        infinite: false,
        // autoplay:true,
        arrows: true,
        speed: 300,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='slick-prev customer_left_button pull-left'><svg width=\"12\" height=\"21\" viewBox=\"0 0 12 21\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M0.99367 10.8711L10.3491 20.098C10.6861 20.43 11.2317 20.43 11.5686 20.098C11.9056 19.7659 11.9056 19.2268 11.5686 18.8956L2.75801 10.2053L11.5687 1.51592C11.9057 1.18385 11.9057 0.64563 11.5687 0.312704C11.2317 -0.0193634 10.686 -0.0193633 10.3491 0.312704L0.99457 9.53949C0.808978 9.722 0.736293 9.9662 0.755116 10.2053C0.735392 10.4444 0.808947 10.6886 0.99367 10.8711Z\" fill=\"white\"/>\n" +
            "</svg>\n</button>",
        nextArrow:"<button type='button' class='slick-next pull-right  customer_right_button'><svg width=\"12\" height=\"21\" viewBox=\"0 0 12 21\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
            "<path d=\"M11.0217 9.53957L1.66631 0.312748C1.32933 -0.0192883 0.783687 -0.0192883 0.446739 0.312748C0.109758 0.644816 0.109758 1.18389 0.446739 1.5151L9.25737 10.2054L0.446705 18.8948C0.109725 19.2269 0.109725 19.7651 0.446705 20.098C0.783686 20.4301 1.32933 20.4301 1.66628 20.098L11.0208 10.8712C11.2064 10.6887 11.2791 10.4445 11.2603 10.2054C11.28 9.96627 11.2064 9.72207 11.0217 9.53957Z\" fill=\"#333333\"/>\n" +
            "</svg>\n</button>",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    variableWidth: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                },

            },
            {
                breakpoint: 768,
                settings: {
                    variableWidth: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 680,
                settings: {
                    variableWidth: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            }]
    });
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 1) {
            $("header").addClass("header_active");
        }else{
            $("header").removeClass("header_active");

        }
    });
    $('.solutions_item_accordion_btn:first-child').addClass('solutions_item_accordion_btn_active');

$('.mobile_message_burger').on('click',()=>{
    $('.mobile_message_links ').toggleClass('mobile_message_links_active')
})
});
$('#first').css('display','block')
function tabs(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");

    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" solutions_item_accordion_btn_active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " solutions_item_accordion_btn_active";
}
